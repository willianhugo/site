<?php
session_start();
function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

$nome = strip_tags(trim($_POST["inputNome"]));
$email = strip_tags(trim($_POST['inputEmail']));
$telefone = strip_tags(trim($_POST['inputTelefone']));
$cidade = strip_tags(trim($_POST['inputCidade']));
$estado = strip_tags(trim($_POST['inputEstado']));
$arquivo = $_FILES['inputArquivo'];

$tmp_name = $_FILES['inputArquivo']['tmp_name'];

$name = 'Curriculo ' . tirarAcentos($nome);
//$storage_path = $_SERVER['DOCUMENT_ROOT'] . '/site/data/' .$name;
//move_uploaded_file($tmp_name, $storage_path);

$tamanho = 1000000;
$tipos = array ('application/pdf');

if($arquivo['size'] > $tamanho){
    print 'O limite do tammando do arquivo é de 1MB.';
}elseif(!in_array($arquivo['type'], $tipos)){
    print 'O tipo do arquivo permitido é PDF.';
}

// Inclui o arquivo class.phpmailer.php localizado na mesma pasta do arquivo php 
require "PHPMailer-master/PHPMailerAutoload.php"; 
 
// Inicia a classe PHPMailer 
$mail = new PHPMailer(); 
 
$mail->SetLanguage ('br');

// Charset (opcional) 
$mail->CharSet = 'UTF-8'; 

// Método de envio 
$mail->IsSMTP(); 
 
// Enviar por SMTP 
$mail->Host = "smtp.hostinger.com.br"; 
 
// Usar autenticação SMTP (obrigatório) 
$mail->SMTPAuth = true; 
$mail->SMTPSecure = 'tls';
//$mail->SMTPAutoTLS = false;

// Você pode alterar este parametro para o endereço de SMTP do seu provedor 
$mail->Port = 587; 
 
// Usuário do servidor SMTP (endereço de email) 
// obs: Use a mesma senha da sua conta de email 
$mail->Username = 'mensagens@protecaomaxima.com.br'; 
$mail->Password = '4sWKENcMG0';

//Prioridade do e-mail
$mail->Priority=1;
 
// Configurações de compatibilidade para autenticação em TLS 
//$mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) ); 
 
// Você pode habilitar esta opção caso tenha problemas. Assim pode identificar mensagens de erro. 
$mail->SMTPDebug = 0; 
 
// Define o remetente 
// Seu e-mail 
//$mail->From = "mensagens@protecaomaxima.com.br"; 
$mail->setFrom ('mensagens@protecaomaxima.com.br','Site');

// Seu nome 
//$mail->FromName = "Site"; 
 
// Define o(s) destinatário(s) 
$mail->AddAddress('curriculo@protecaomaxima.com.br', 'Currículos Proteção Máxima'); 
 
// Opcional: mais de um destinatário
// $mail->AddAddress('fernando@email.com'); 
 
// Opcionais: CC e BCC
// $mail->AddCC('joana@provedor.com', 'Joana'); 
// $mail->AddBCC('roberto@gmail.com', 'Roberto'); 
 
// Definir se o e-mail é em formato HTML ou texto plano 
// Formato HTML . Use "false" para enviar em formato texto simples ou "true" para HTML.
$mail->IsHTML(true); 
 
// Assunto da mensagem 
$mail->Subject = "Currículo - {$nome}";
 
// Corpo do email 

$mail->Body = "<strong>Nome: </strong>{$nome} <br/> <strong>Email: </strong>{$email} <br/> <strong>Telefone: </strong>{$telefone} <br/> <strong>Cidade: </strong>{$cidade} <br/> <strong>Estado: </strong>{$estado} <br/>";
//Corpo do email em texto
//$mail->AltBody = "Nome: {$nome} <br> Email: {$email} <br> Telefone: {$telefone} <br> Cidade: {$cidade} <br> Estado: {$Estado} <br> Empresa: {$empresa} <br> Serviço: {$servico} <br> Mensagem: {$mensagem}"; 
 
// Opcional: Anexos 
$mail->AddAttachment($tmp_name, $name, $encoding='base64', $tipos[0]);

// Envia o e-mail 
$enviado = $mail->Send(); 
// Exibe uma mensagem de resultado 
if ($enviado) 
{ 
    $_SESSION['mensagem']='Sua mensagem foi enviada com sucesso!';
    header('Location: '.'/site/trabalheconosco.php');
} else { 
    echo 'Houve um erro durante o envio da mensagem.'.$mail->ErrorInfo; 
} 
?>